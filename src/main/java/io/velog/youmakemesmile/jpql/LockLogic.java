package io.velog.youmakemesmile.jpql;


import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

@Component
public class LockLogic {

    private static final String JAVAX_PERSISTENCE_LOCK_TIMEOUT = "javax.persistence.lock.timeout";

    public void lock(EntityManager entityManager, Object enetityToLock, Integer lockTimeout) {
        if(entityManager != null){
            try{
                entityManager.setProperty(JAVAX_PERSISTENCE_LOCK_TIMEOUT, lockTimeout);
                entityManager.lock(enetityToLock, LockModeType.PESSIMISTIC_WRITE);
            } catch (Exception e){
                entityManager.clear();
            }
        }
    }
}
