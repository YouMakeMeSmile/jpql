package io.velog.youmakemesmile.jpql;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Setter
@Getter
@MappedSuperclass
public class Audit {
    private LocalDateTime localDateTime;
    private LocalDateTime updateDateTime;
}
