package io.velog.youmakemesmile.jpql;


import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.dialect.MySQLDialect;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(
        basePackages = "io.velog.youmakemesmile.jpql.temp",
        entityManagerFactoryRef = "tempEntityManagerFactory",
        transactionManagerRef = "tempTransactionManager"
)
@EnableTransactionManagement
public class TempDataSourceConfig {
    @Bean
    @Primary // 메인으로 사용될 DataSource
    @ConfigurationProperties("spring.datasource")
    public DataSourceProperties tempDataSourceProperties() {
        return new DataSourceProperties();
    }


    @Bean
    @Primary // 메인으로 사용될 DataSource
    @ConfigurationProperties("spring.datasource.hikari")
    public HikariDataSource tempDataSource() {
        return tempDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean
    @Primary // 메인으로 사용될 DataSource
    @ConfigurationProperties("spring.jpa")
    public JpaProperties tempJpaProperties() {
        return new JpaProperties();
    }

    @Bean
    @Primary // 메인으로 사용될 DataSource
    public LocalContainerEntityManagerFactoryBean tempEntityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

//        vendorAdapter.setGenerateDdl(true);
//        Properties props = new Properties();  // Properties에 Hibernate Config 설정 추가
//        props.setProperty("hibernate.metadata_builder_contributor", "io.velog.youmakemesmile.jpql.MyMetadataBuilderContributor");
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("io.velog.youmakemesmile.jpql.temp");
        factory.setDataSource(tempDataSource());

        factory.setJpaPropertyMap(tempJpaProperties().getProperties());
        return factory;
    }

    @Bean
    @Primary // 메인으로 사용될 DataSource
    public PlatformTransactionManager tempTransactionManager(@Qualifier("tempEntityManagerFactory") EntityManagerFactory entityManagerFactory) {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory);
        return txManager;
    }


}
