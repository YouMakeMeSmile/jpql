package io.velog.youmakemesmile.jpql.test;

import io.velog.youmakemesmile.jpql.temp.User;
import io.velog.youmakemesmile.jpql.temp.UserDto;
import io.velog.youmakemesmile.jpql.temp.UserVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TestUserRepository extends JpaRepository<TestUser, String> {


}
