package io.velog.youmakemesmile.jpql.test;

import io.velog.youmakemesmile.jpql.Audit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "test_user")
@AllArgsConstructor
@DynamicUpdate
public class TestUser extends Audit {
    @Id
    private String id;
    private String name;
    private String phone;
    private String registerInfo;
    private String deptId;
}
