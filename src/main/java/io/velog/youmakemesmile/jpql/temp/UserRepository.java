package io.velog.youmakemesmile.jpql.temp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {

    @Query(value = "select user " +
            "from User user " +
            "where user.name = :name")
    List<User> findByName(@Param("name") String name);

    @Query(value = "select " +
            "new io.velog.youmakemesmile.jpql.temp.UserDto(user.id, user.name, user.phone, user.deptId, dept.name) " +
            "from User user " +
            "left outer join Dept dept on user.deptId = dept.id ")
    List<UserDto> findUserDept();

    @Query(value = "select max(user.id) " +
            "from User user " +
            "where user.deptId is not null")
    String findMaxUserId();


    @Query(value = "select function('date_format', :date, '%Y/%m/%d') " +
            "from User user ")
    String findNow(@Param("date") LocalDateTime date);


    @Query(value = "select user " +
            "from User user " +
            "where function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.id')) = 'admin' ")
    List<User> findAllByRegisterAdmin();


    @Query(value = "select user " +
            "from User user " +
            "where function('MATCH_AGAINST', user.name, :name) >0 ")
    List<User> findAllByName(@Param("name") String name);


    @Query(value = "select " +
            "new io.velog.youmakemesmile.jpql.temp.UserVo(user.id, user.name, user.phone, function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.id')), function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), function('date_format', function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), '%Y/%m/%d'), user.deptId, dept.name) " +
            "from User user " +
            "left outer join Dept dept on user.deptId = dept.id ")
    List<UserVo> findTest();

    @Query(value = "select " +
            "new io.velog.youmakemesmile.jpql.temp.UserVo(user.id, user.name, user.phone, function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.id')), function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), function('date_format', function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), '%Y/%m/%d'), user.deptId, dept.name) " +
            "from User user " +
            "left outer join Dept dept on user.deptId = dept.id " +
            "where function('MATCH_AGAINST', user.name, :name) > 0")
    List<UserVo> findTest2(@Param("name") String name);

    @Query(value = "select user.id as id, user.name as name, user.phone as phone, json_unquote(json_extract(user.register_info,'$.id')) as registerInfo , str_to_date(json_unquote(json_extract(user.register_info, '$.date')), '%Y-%m-%d') as registerDate, user.dept_id as deptId, dept.name as deptName " +
            "from user user " +
            "left outer join dept dept on user.dept_id = dept.id ", nativeQuery = true)
    List<UserNativeVo> findTest3();


    @Query(value = "select user.id as id, user.name as name, user.phone as phone, json_unquote(json_extract(user.register_info,'$.id')) as registerInfo , str_to_date(json_unquote(json_extract(user.register_info, '$.date')), '%Y-%m-%d') as registerDate, user.dept_id as deptId, dept.name as deptName " +
            "from user user " +
            "left outer join dept dept on user.dept_id = dept.id " +
            "where match (user.name) against (:name in boolean mode) > 0", nativeQuery = true)
    List<UserNativeVo> findTest4(@Param("name") String name);


    interface UserNativeVo {
        String getId();

        String getName();

        String getPhone();

        String getRegisterInfo();

        String getRegisterDate();

        String getDeptId();

        String getDeptName();
    }

}
