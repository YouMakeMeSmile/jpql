package io.velog.youmakemesmile.jpql.temp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserVo {
    private String id;
    private String name;
    private String phone;
    private String registerId;
    private LocalDate registerDate1;
    private String registerDate2;
    private String deptId;
    private String deptName;


//    public static void main(String[] args) {
////        int[] solution = new UserVo().solution(new String[]{"muzi", "frodo", "apeach", "neo"}, new String[]{"muzi frodo", "apeach frodo", "frodo neo", "muzi neo", "apeach muzi"}, 2);
//        int[] solution = new UserVo().solution(new String[]{"con", "ryan"}, new String[]{"ryan con", "ryan con", "ryan con", "ryan con"}, 3);
//
//        System.out.println();
//    }
//
//
//    public int[] solution(String[] id_list, String[] report, int k) {
//        List<List<String>> collect = Arrays.asList(report).stream().map(value -> Arrays.asList(value.split(" "))).distinct().collect(Collectors.toList());
//        Map<String, List<String>> reporterMap = new HashMap<>();
//        Map<String, Integer> targetMap = new HashMap<>();
//        for (List<String> s : collect) {
//            String reporter = s.get(0);
//            String target = s.get(1);
//            if (reporterMap.containsKey(reporter)) {
//                reporterMap.get(reporter).add(target);
//            } else {
//                List<String> list = new ArrayList<>();
//                list.add(target);
//                reporterMap.put(reporter, list);
//            }
//
//            if (targetMap.containsKey(target)) {
//                int i = targetMap.get(target) + 1;
//                targetMap.put(target, i);
//            } else {
//                targetMap.put(target, 1);
//            }
//        }
//        int[] ints = new int[id_list.length];
//        for (int j = 0; j < id_list.length; j++) {
//            List<String> strings = reporterMap.get(id_list[j]);
//            int i = 0;
//            if (strings != null) {
//                for (String s1 : strings) {
//                    Integer integer = targetMap.get(s1);
//                    if (integer != null && integer >= k) {
//                        i++;
//                    }
//                }
//            }
//            ints[j] = i;
//        }
//        return ints;
//    }

    public static void main(String[] args) {
        new UserVo().solution(437674, 3);
    }

    public int solution(int n, int k) {
        int answer = 0;
        String str = conversion(n, k);
        List<String> split = Arrays.stream(str.split("0")).filter(value -> !"".equals(value)).collect(Collectors.toList());
        for (String s : split) {
            if (primenumber(Integer.parseInt(s))) answer++;
        }
        return answer;
    }

    public static String conversion(int number, int N) {
        StringBuilder sb = new StringBuilder();
        int current = number;

        // 진법 변환할 숫자가 0보다 큰 경우 지속 진행
        while (current > 0) {
            // 만약 N으로 나누었는데 10보다 작다면 해당 숫자를 바로 append
            if (current % N < 10) {
                sb.append(current % N);

                // 만약 N이 10보다 큰 경우, N으로 나누었는데 10 이상이면 A, B등으로 표현하므로 기존 숫자는 10진법이므로 10만큼 빼고 'A'를 더한다.
                // 왜냐면 1~9까지는 숫자로 표기하지만, 10 부터는 'A', 'B' 순서로 나타내기 때문이다.
                // 나머지가 10이라면 'A' + 10이 아니라 'A'로 나타내야 하기 때문
            } else {
                sb.append((char) (current % N - 10 + 'A'));
            }
            current /= N;
        }
        return sb.reverse().toString();
        // StringBuilder의 reverse를 사용해야 정상적으로 출력 가능. 안그러면 거꾸로 출력됨
//        System.out.println("숫자 : " + number + "를 " + N + "진법으로 변환한 수 : " + sb.reverse());
    }

    static boolean primenumber(int n) {
        if (n == 1) {
            return false;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;

    }
}
