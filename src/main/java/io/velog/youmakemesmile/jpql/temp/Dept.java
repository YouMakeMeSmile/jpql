package io.velog.youmakemesmile.jpql.temp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.dialect.MySQL8Dialect;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "dept")
@AllArgsConstructor
public class Dept {
    @Id
    private String id;
    private String name;
}
