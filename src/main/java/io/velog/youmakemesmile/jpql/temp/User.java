package io.velog.youmakemesmile.jpql.temp;

import io.velog.youmakemesmile.jpql.Audit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "user")
@AllArgsConstructor
@DynamicUpdate
public class User extends Audit {
    @Id
    private String id;
    private String name;
    private String phone;
    private String registerInfo;
    private String deptId;
}
