package io.velog.youmakemesmile.jpql;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class InsertEventListener implements PreInsertEventListener {

    @PersistenceContext
    private EntityManager entityManager;

    @PersistenceContext(unitName = "test")
    private EntityManager entityManager2;


    @Autowired
    private AuditLogic auditLogic;

    @PostConstruct
    private void init() {
        SessionFactoryImpl sessionFactory = this.entityManager.getEntityManagerFactory().unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(this);

        SessionFactoryImpl sessionFactory1 = this.entityManager2.getEntityManagerFactory().unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry1 = sessionFactory1.getServiceRegistry().getService(EventListenerRegistry.class);
        registry1.getEventListenerGroup(EventType.PRE_INSERT).appendListener(this);
    }

    @Override
    public boolean onPreInsert(PreInsertEvent event) {
        this.auditLogic.setAuditIfTarget(event);
        return false;
    }
}
