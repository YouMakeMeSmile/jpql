package io.velog.youmakemesmile.jpql;

import io.velog.youmakemesmile.jpql.temp.User;
import io.velog.youmakemesmile.jpql.temp.UserDto;
import io.velog.youmakemesmile.jpql.temp.UserRepository;
import io.velog.youmakemesmile.jpql.temp.UserVo;
import io.velog.youmakemesmile.jpql.test.TestUser;
import io.velog.youmakemesmile.jpql.test.TestUserRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
@RestController
@RequiredArgsConstructor
@Transactional
public class JpqlApplication {

    private final UserRepository userRepository;
    private final TestUserRepository testUserRepository;


    @PersistenceContext
    private EntityManager entityManager;

    public static void main(String[] args) {
        SpringApplication.run(JpqlApplication.class, args);
    }

    @GetMapping()
    public String test(@RequestParam(defaultValue = "kim") String name) {
//        String now = userRepository.findNow(LocalDateTime.now());
//        List<User> allByRegisterAdmin = userRepository.findAllByRegisterAdmin();
//        List<User> le = userRepository.findAllByName("le");
//
//        List<User> resultList = entityManager
//                .createQuery("select user from User user where function('MATCH_AGAINST', user.name, :name) > 0 ", User.class)
//                .setParameter("name", "le")
//                .getResultList();
//
//        List<UserDto> resultList2 = entityManager
//                .createQuery("select " +
//                        "new io.velog.youmakemesmile.jpql.temp.UserDto(user.id, user.name, user.phone, user.deptId, dept.name) " +
//                        "from User user " +
//                        "left outer join Dept dept on user.deptId = dept.id ", UserDto.class)
//                .getResultList();
        userRepository.save(new User("123","123","123","123","123"));
        try {
            testUserRepository.save(new TestUser("456", "456", "456", "456", "456"));
            List<TestUser> all = testUserRepository.findAll();
            System.out.println();
        }catch (Exception e){
            System.out.println();
        }
        return userRepository.findMaxUserId();
    }

    @GetMapping("/99")
    public String test99(@RequestParam(defaultValue = "kim") String name) {
//        String now = userRepository.findNow(LocalDateTime.now());
//        List<User> allByRegisterAdmin = userRepository.findAllByRegisterAdmin();
//        List<User> le = userRepository.findAllByName("le");
//
//        List<User> resultList = entityManager
//                .createQuery("select user from User user where function('MATCH_AGAINST', user.name, :name) > 0 ", User.class)
//                .setParameter("name", "le")
//                .getResultList();
//
//        List<UserDto> resultList2 = entityManager
//                .createQuery("select " +
//                        "new io.velog.youmakemesmile.jpql.temp.UserDto(user.id, user.name, user.phone, user.deptId, dept.name) " +
//                        "from User user " +
//                        "left outer join Dept dept on user.deptId = dept.id ", UserDto.class)
//                .getResultList();
        userRepository.save(new User("123","kim","123","123","123"));
        try {
            testUserRepository.save(new TestUser("456", "lee", "456", "456", "456"));
            List<TestUser> all = testUserRepository.findAll();
            System.out.println();
        }catch (Exception e){
            System.out.println();
        }
        return userRepository.findMaxUserId();
    }

    @GetMapping("/1")
    public List<UserVo> test1() {
        return userRepository.findTest();
    }

    @GetMapping("/2")
    public List<UserVo> test2(@RequestParam(defaultValue = "kim") String name) {
        return userRepository.findTest2(name);
    }

    @GetMapping("/post")
    public void post() {
        userRepository.save(new User("999", "999", "999", "999", "999"));
        testUserRepository.save(new TestUser("999", "999", "999", "999", "999"));
    }

    @GetMapping("/3")
    public List<UserVo> test3() {
        return entityManager.createQuery("select " +
                "new io.velog.youmakemesmile.jpql.temp.UserVo(user.id, user.name, user.phone, function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.id')), function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), function('DATE_FORMAT', function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), '%Y/%m/%d'), user.deptId, dept.name) " +
                "from User user " +
                "left outer join Dept dept on user.deptId = dept.id ", UserVo.class)
                .getResultList();
    }

    @GetMapping("/4")
    public List<UserVo> test4(@RequestParam(defaultValue = "kim") String name) {
        return entityManager.createQuery("select " +
                "new io.velog.youmakemesmile.jpql.temp.UserVo(user.id, user.name, user.phone, function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.id')), function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), function('DATE_FORMAT', function('STR_TO_DATE',function('JSON_UNQUOTE',function('JSON_EXTRACT',user.registerInfo,'$.date')),'%Y-%m-%d'), '%Y/%m/%d'), user.deptId, dept.name) " +
                "from User user " +
                "left outer join Dept dept on user.deptId = dept.id " +
                "where function('MATCH_AGAINST', user.name, :name) > 0", UserVo.class)
                .setParameter("name", name)
                .getResultList();
    }


    @GetMapping("/5")
    public List<UserRepository.UserNativeVo> test5() {
        return userRepository.findTest3();
    }

    @GetMapping("/6")
    public List<UserRepository.UserNativeVo> test6(@RequestParam(defaultValue = "kim") String name) {
        return userRepository.findTest4(name);
    }

    @GetMapping("/7")
    public List<UserDto> test7(){
        return entityManager.createNativeQuery("select user.id as id, user.name as name, user.phone as phone, user.dept_id as deptId, dept.name as deptName " +
                "from user user " +
                "left outer join dept dept on user.dept_id = dept.id " +
                "where match (user.name) against (:name in boolean mode) > 0").setParameter("name","le")
                .unwrap(NativeQuery.class)
                .setResultTransformer(Transformers.aliasToBean(UserDto.class))
                .getResultList();
    }


}
