package io.velog.youmakemesmile.jpql;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.EntityEntry;
import org.hibernate.engine.spi.Status;
import org.hibernate.event.internal.DefaultFlushEntityEventListener;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventSource;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.FlushEntityEvent;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class UpdateLockAndAuditEventListener extends DefaultFlushEntityEventListener {

    @Value("${spring.datasource.lockTimeout:0}")
    private Integer LOCK_WAIT_TIMEOUT;

    @PersistenceContext
    private EntityManager entityManager;

    @PersistenceContext(unitName = "test")
    private EntityManager entityManager2;

    @Autowired
    private AuditLogic auditLogic;

    @Autowired
    private LockLogic lockLogic;

    @PostConstruct
    private void init() {
        SessionFactoryImpl sessionFactory = entityManager.getEntityManagerFactory().unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.setListeners(EventType.FLUSH_ENTITY, this);

        SessionFactoryImpl sessionFactory1 = this.entityManager2.getEntityManagerFactory().unwrap(SessionFactoryImpl.class);
        EventListenerRegistry registry1 = sessionFactory1.getServiceRegistry().getService(EventListenerRegistry.class);
        registry1.setListeners(EventType.FLUSH_ENTITY, this);
    }

    @Override
    protected void dirtyCheck(FlushEntityEvent event) throws HibernateException {
        super.dirtyCheck(event);

        final Object entity = event.getEntity();
        final EntityEntry entityEntry = event.getEntityEntry();
        final EventSource session = event.getSession();
        final EntityPersister persister = entityEntry.getPersister();
        final Status status = entityEntry.getStatus();
        final int[] dirtyProperties = event.getDirtyProperties();

        if(isUpdateTarget(dirtyProperties, status)){
            if(session.getPersistenceContext().getEntry(event.getEntity()).isExistsInDatabase()){
                this.lockLogic.lock(entityManager, entity, LOCK_WAIT_TIMEOUT);
            }

            auditLogic.setAuditIfTarget(entity);
            event.setPropertyValues(persister.getPropertyValues(entity));
            event.setDirtyProperties(persister.findDirty(event.getPropertyValues(),entityEntry.getLoadedState(),entity, session));
        }

    }

    private boolean isUpdateTarget(int[] dirtyProperties, Status status) {
        return dirtyProperties != null && dirtyProperties.length > 0 && Status.MANAGED.equals(status);
    }
}
