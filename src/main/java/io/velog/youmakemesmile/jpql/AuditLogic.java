package io.velog.youmakemesmile.jpql;

import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Component
public class AuditLogic {

    public void setAuditIfTarget(PreInsertEvent preInsertEvent) {
        EntityPersister persister = preInsertEvent.getPersister();

        int i = persister.getEntityMetamodel().getPropertyIndex("localDateTime");
        Object[] states = preInsertEvent.getState();
        Object entity = preInsertEvent.getEntity();

        LocalDateTime now = LocalDateTime.now();
        setAuditProperty(persister, states, entity, "localDateTime", now);
        setAuditProperty(persister, states, entity, "updateDateTime", now);

        //        persister.setPropertyValue(preInsertEvent.getEntity(),i,value);
        //        persister.setPropertyValues(preInsertEvent.getEntity(), objects);
    }

    public void setAuditIfTarget(Object entity) {
        Audit audit = (Audit) entity;
        audit.setUpdateDateTime(LocalDateTime.now());
    }

    private void setAuditProperty(EntityPersister persister, Object[] states, Object parentJpo, String field, Object value) {
        int index = persister.getEntityMetamodel().getPropertyIndex(field);
        states[index] = value;
        invokeSetter(parentJpo, field, value);
    }

    private void invokeSetter(Object parentJpo, String field, Object value) {
        if(value == null){
            return;
        }

        String setter = String.format("set%s%s", field.substring(0,1).toUpperCase(), field.substring(1));
        try{
            Method method = parentJpo.getClass().getMethod(setter, value.getClass());
            method.invoke(parentJpo,value);
        } catch (Throwable e){
            System.out.println();
        }
    }
}
